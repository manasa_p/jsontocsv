import csv
import json
# did a pip install of gspread
import gspread
# my credential json from Google API
gc = gspread.service_account("token.json")

# Input json file
input_file = open('input.json')
input_json = json.load(input_file)
input_file.close()

cols_list = list(set([item for sublist in input_json.values() for item in sublist]))

# writing to a csv
with open("test.csv", 'w', newline='') as csvfile:
    writer = csv.DictWriter(csvfile, fieldnames=['name']+cols_list)
    writer.writeheader()
    for k, v in input_json.items():
        row_dict = {'name': k}
        for value in cols_list:
            if value in v:
                row_dict[value] = 1
            else:
                row_dict[value] = 0
        writer.writerow(row_dict)

# creating a spreadsheet
spreadsheet = gc.create("CSVData.csv")
content = open('test.csv', 'r').read()
# importing created csv to the spreadsheet
gc.import_csv(spreadsheet.id, content)
# used a service account to use the api, so sharing with my own email for Drive access
spreadsheet.share('manasapatibandla92@gmail.com', perm_type='user', role='writer')
