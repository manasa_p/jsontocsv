JSON To CSV
-----------

The script JsonToCsv.py contains code that can read an input json file, and write the json data to csv in the requested format.
Furthermore, Google Drive API is used to create a spreadsheet in Google Drive. The created csv is imported into the spreadsheet.
A PyPI package called "gspread" is made use of for this purpose of connecting to Google Drive. 

In the script, I used my own service account and its credentials to create the spreadsheet. I used another functionality to share the spreadsheet with my actual email, so I can now access the created spreadsheet in my own Google Drive.
You can change the email to your email id, to share it with yourself to see the csv file. 
